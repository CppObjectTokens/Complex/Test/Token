# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

set(CppOtlApplicationTokenCompletePath ${CMAKE_CURRENT_SOURCE_DIR}/application/Token/Complete)

add_subdirectory(${CppOtlApplicationTokenCompletePath}/Suite application/Token/Complete/Suite)
