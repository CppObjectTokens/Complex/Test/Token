# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

set(CppOtlLibraryPath ${CMAKE_CURRENT_SOURCE_DIR}/library)

add_subdirectory(${CppOtlLibraryPath}/CppOtl library/CppOtl)
