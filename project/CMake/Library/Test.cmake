# Copyright (c) 2018-2020 Viktor Kireev
# Distributed under the MIT License

set(CppOtlLibraryTestPath ${CMAKE_CURRENT_SOURCE_DIR}/library/Test)

add_subdirectory(${CppOtlLibraryTestPath}/Tool/Sample library/Test/Tool/Sample)
add_subdirectory(${CppOtlLibraryTestPath}/Tool/Utility library/Test/Tool/Utility)
add_subdirectory(${CppOtlLibraryTestPath}/Detail/Token library/Test/Detail/Token)
add_subdirectory(${CppOtlLibraryTestPath}/Binding/Stl library/Test/Binding/Stl)
add_subdirectory(${CppOtlLibraryTestPath}/Binding/Otl/Suite library/Test/Binding/Otl/Suite)
