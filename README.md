# CppOtl tests

Tests for the [C++ Object Token Library](https://gitlab.com/CppObjectTokens/Module/Library/CppOtl).

CppOtl uses [Catch2](https://github.com/catchorg/Catch2) for tests.

### Clone

```
git clone --recurse-submodules https://gitlab.com/CppObjectTokens/Complex/Test/Token.git
cd Token
git submodule update --remote --recursive
```

### Configure

- [`application/Token/Complete/Suite/include/Test/Suite/Config.h`](https://gitlab.com/CppObjectTokens/Module/Test/Subject/Token/Complete/Suite/blob/master/include/Test/Suite/Config.h) - Enable/disable test cases.
- [`application/Token/Complete/Suite/include/Test/Suite/Preset.h`](https://gitlab.com/CppObjectTokens/Module/Test/Subject/Token/Complete/Suite/blob/master/include/Test/Suite/Preset.h) - Setup test subjects.

### Build

Use CMake to build tests.
